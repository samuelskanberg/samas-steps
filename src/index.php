<?php 

session_start();

require __DIR__ . '/controllers/session.php';
require __DIR__ . '/controllers/db.php';

$request = $_SERVER['REQUEST_URI'];

// Route for '/'
if (preg_match('/^\/$/', $request)) {
    require __DIR__ . '/views/index.php';
}
// Route for ''
else if (preg_match('/^$/', $request)) {
    require __DIR__ . '/views/index.php';
}
// Handle all trailing '/' like '/add/'
else if (preg_match('/^\/\w+\/$/', $request)) {
    $trimmed_route = substr($request, 0, -1);
    header("Location: " . $trimmed_route);
}

// Route for '/add'
else if (preg_match('/^\/add$/', $request)) {
    require __DIR__ . '/views/add.php';
}

// Route for '/delete/x'
else if (preg_match('/^\/delete\/(\d+)$/', $request, $matches, PREG_OFFSET_CAPTURE)) {
    $delete_id = intval($matches[1][0]);
    // var_dump($id);
    require __DIR__ . '/views/delete.php';
}
// Route for '/login'
else if (preg_match('/^\/login$/', $request)) {
    require __DIR__ . '/views/login.php';
}
// Route for '/logout'
else if (preg_match('/^\/logout$/', $request)) {
    require __DIR__ . '/views/logout.php';
}
// Route for '/users'
else if (preg_match('/^\/users$/', $request)) {
    require __DIR__ . '/views/users.php';
}
// Route for '/users/create'
else if (preg_match('/^\/users\/create$/', $request)) {
    require __DIR__ . '/views/users_create.php';
}
// Route for '/users/delete/x'
else if (preg_match('/^\/users\/delete\/(\d+)$/', $request, $matches, PREG_OFFSET_CAPTURE)) {
    $user_delete_id = intval($matches[1][0]);
    require __DIR__ . '/views/users_delete.php';
}
// Route for '/users/edit/x'
else if (preg_match('/^\/users\/edit\/(\d+)$/', $request, $matches, PREG_OFFSET_CAPTURE)) {
    $user_edit_id = intval($matches[1][0]);
    require __DIR__ . '/views/users_edit.php';
}
// All other
else {
    http_response_code(404);
    require __DIR__ . '/views/404.php';
}

?>
