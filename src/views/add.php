<?php
    if (!is_logged_in()) {
        header("Location: /login");
        exit();
    }
    $uid = session_get_uid();
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $number_steps = $_POST['number_steps'] ?? null;
        $date_steps = $_POST['date_steps'] ?? null;
        if ($number_steps != null && is_numeric($number_steps)) {
            $result = add_steps($uid, $number_steps, $date_steps);
            if ($result) {
                $info = array(
                    "message" => "Steg tillagda. " . $date_steps . ": " . $number_steps,
                    "type" => "info"
                );
            } else {
                $info = array(
                    "message" => "Något fel uppstod",
                    "type" => "error"
                );
            }

        }
    }
?>

<?php include './components/page_start.php'; ?>
<?php include './components/header.php'; ?>
<div class="container">
    <h1>Lägg till steg för <?php echo session_get_name(); ?></h1>
    <?php
        if (isset($info)) {
            if ($info['type'] == "error") {
                echo '<div class="alert alert-danger">';
            } else {
                echo '<div class="alert alert-success">';
            }
            echo $info['message'];
            echo '</div>';
        }
    ?>
    <form method="post">
        <div class="form-group">
            <label for="sum">Antal steg</label>
            <input type="number" class="form-control" name="number_steps">
            <input type="date" class="form-control" value="<?php echo date('Y-m-d',strtotime("-1 days")); ?>" name="date_steps">
        </div>
        <button type="submit" class="btn btn-primary mt-4">Lägg till</button>
    </form>

    <h2>Senast inlagda steg</h2>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">Datum</th>
                <th scope="col">Steg</th>
                <th scope="col">Ta bort</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $steps = list_steps_for_user($uid);
                foreach($steps as $step) {
                    echo "<tr>";
                    echo "<td>".$step['date_steps']."</td>";
                    echo "<td>".$step['number_steps']."</td>";
                    echo '<td><a href="/delete/'.$step['id'].'">Ta bort '.$step['sum']."</td>";
                    echo "</tr>";
                }
            ?>
        </tbody>
    </table>
</div>

<?php include './components/page_end.php'; ?>
