<?php 
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $email = $_POST['email'] ?? null;
        $password = $_POST['password'] ?? null;
        $result = verify_password($email, $password);
        if ($result["password_match"]) {
            session_login($result["name"], $result["uid"], $result["is_admin"]);
            header("Location: /");
            exit();
        } else {
            $info = array(
                "message" => "Inloggning misslyckades",
                "type" => "error"
            );
        }
    }
?>

<?php include './components/page_start.php'; ?>
<?php include './components/header.php'; ?>
<div class="container">
    <?php
        if (isset($info)) {
            if ($info['type'] == "error") {
                echo '<div class="alert alert-danger">';
            } else {
                echo '<div class="alert alert-success">';
            }
            echo $info['message'];
            echo '</div>';
        }
    ?>

    <form method="post">
        <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <input type="email" class="form-control" name="email">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Lösenord</label>
            <input type="password" class="form-control" name="password">
        </div>
        <button type="submit" class="btn btn-primary mt-4">Logga in</button>
    </form>

</div>

<?php include './components/page_end.php'; ?>
