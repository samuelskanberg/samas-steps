<?php
    $uid = session_get_uid();
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $id = $_POST['delete_id'] ?? null;
        $step = get_step($id, $uid);
        if ($step['id'] != null) {
            $res = delete_step($step['id'], $uid);
            $info = array(
                "message" => "Steg borttagna",
                "type" => "info"
            );
        }
    } else {
        $step = get_step($delete_id, $uid);
    }
?>

<?php include './components/page_start.php'; ?>
<?php include './components/header.php'; ?>
<div class="container">
    <h1>Ta bort steg</h1>
    <?php if (isset($info)): ?>
        <?php
            if ($info['type'] == "error") {
                echo '<div class="alert alert-danger">';
            } else {
                echo '<div class="alert alert-success">';
            }
            echo $info['message'];
            echo '</div>';
        ?>
        <a href="/add">Lägg till fler steg</a>
    <?php else: ?>
        <?php if ($step['id'] == null): ?>
            Det här steget finns inte.
        <?php else: ?>
            <table class="table">
            <thead>
                <tr>
                    <th scope="col">Datum</th>
                    <th scope="col">Steg</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $step['date_steps']; ?> kr</td>
                    <td><?php echo $step['number_steps']; ?></td>
                </tr>
            </tbody>
        </table>
            <form method="post">
                <input type="hidden" class="form-control" name="delete_id" value="<?php echo $delete_id; ?>">
                <button type="submit" class="btn btn-primary mt-4">Ta bort</button>
            </form>
        <?php endif; ?>
    <?php endif; ?>
</div>

<?php include './components/page_end.php'; ?>