<?php
    if (!is_logged_in()) {
        header("Location: /login");
        exit();
    }
    if (!is_admin()) {
        header("Location: /");
        exit();
    }
?>

<?php include './components/page_start.php'; ?>
<?php include './components/header.php'; ?>
<div class="container">
    <h1>Användare</h1>
    <?php
        if (isset($info)) {
            if ($info['type'] == "error") {
                echo '<div class="alert alert-danger">';
            } else {
                echo '<div class="alert alert-success">';
            }
            echo $info['message'];
            echo '</div>';
        }
    ?>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Namn</th>
                <th scope="col">Email</th>
                <th scope="col">Admin</th>
                <th scope="col">Ändra</th>
                <th scope="col">Ta bort</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $users = list_users();
                foreach($users as $user) {
                    echo "<tr>";
                    echo "<td>".$user['id']."</td>";
                    echo "<td>".$user['name']."</td>";
                    echo "<td>".$user['email']."</td>";
                    echo "<td>".$user['admin']."</td>";
                    echo '<td><a href="/users/edit/'.$user['id'].'">Ändra</a></td>';
                    echo '<td><a href="/users/delete/'.$user['id'].'">Ta bort</a></td>';
                    echo "</tr>";
                }
            ?>
        </tbody>
    </table>

    <a href="/users/create" class="btn btn-primary">Skapa användare</a>

</div>

<?php include './components/page_end.php'; ?>
