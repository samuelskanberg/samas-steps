<?php
    if (!is_logged_in()) {
        header("Location: /login");
        exit();
    }
    if (!is_admin()) {
        header("Location: /");
        exit();
    }
    $uid = session_get_uid();
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $name = $_POST['name'] ?? "";
        $email = $_POST['email'] ?? "";
        $password1 = $_POST['password1'] ?? "";
        $password2 = $_POST['password2'] ?? "";
        $is_admin = strcmp($_POST['admin'] ?? "", "") != 0;

        if (strcmp($password1, $password2) != 0) {
            $info = array(
                "message" => "Lösenorden är olika",
                "type" => "error"
            );
        } else {
            add_user($name, $email, $password1, $is_admin);
            $info = array(
                "message" => "Användare skapad",
                "type" => "info"
            );
        }
    }
?>

<?php include './components/page_start.php'; ?>
<?php include './components/header.php'; ?>
<div class="container">
    <h1>Skapa användare</h1>
    <?php
        if (isset($info)) {
            if ($info['type'] == "error") {
                echo '<div class="alert alert-danger">';
            } else {
                echo '<div class="alert alert-success">';
            }
            echo $info['message'];
            echo '</div>';
        }
    ?>
    <form method="post">
        <div class="form-group">
            <label for="name">Namn</label>
            <input type="text" class="form-control" name="name">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email">
        </div>
        <div class="form-group">
            <label for="admin">Admin</label>
            <input type="checkbox" class="form-check-input" name="admin" value="admin">
        </div>
        <div class="form-group">
            <label for="password1">Lösenord</label>
            <input type="password" class="form-control" name="password1">
        </div>
        <div class="form-group">
            <label for="password2">Lösenord igen</label>
            <input type="password" class="form-control" name="password2">
        </div>
        <button type="submit" class="btn btn-primary mt-4">Skapa</button>
    </form>

</div>

<?php include './components/page_end.php'; ?>
