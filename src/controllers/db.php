<?php

$db_path = __DIR__ . "/samas.db";

function create_database() {
    global $db_path;
    $db = new SQLite3($db_path);

    $db->exec("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, email NOT NULL UNIQUE, name TEXT, password TEXT, admin INTEGER)");
    $db->exec("CREATE TABLE IF NOT EXISTS steps(id INTEGER PRIMARY KEY, uid INT, number_steps INT, date_steps TEXT)");
    $db->close();
}

function list_users() {
    global $db_path;
    $db = new SQLite3($db_path);

    $stm = $db->prepare('SELECT id, name, email, admin from users');
    $res = $stm->execute();
    $users = array();
    while ($row = $res->fetchArray()) {
        $users[] = array(
            "id" => $row['id'],
            "name" => $row['name'],
            "email" => $row['email'],
            "admin" => $row['admin']
        );
    }
    return $users;
}

function add_user($name, $email, $password, $is_admin) {
    global $db_path;
    $db = new SQLite3($db_path);

    $hashed_password = password_hash($password, PASSWORD_BCRYPT);

    if ($is_admin) {
        $admin = 1;
    } else {
        $admin = 0;
    }
    $stm = $db->prepare('INSERT INTO users(email, name, password, admin) VALUES (:email, :name, :password, :admin)');
    $stm->bindValue(':email', $email);
    $stm->bindValue(':name', $name);
    $stm->bindValue(':password', $hashed_password);
    $stm->bindValue(':admin', $admin);
    $res = $stm->execute();

    return boolval($res);
}

function update_user($uid, $name, $email, $is_admin) {
    global $db_path;
    $db = new SQLite3($db_path);

    if ($is_admin) {
        $admin = 1;
    } else {
        $admin = 0;
    }
    $stm = $db->prepare('UPDATE users set email = :email, name = :name, admin = :admin where id = :id');
    $stm->bindValue(':email', $email);
    $stm->bindValue(':name', $name);
    $stm->bindValue(':admin', $admin);
    $stm->bindValue(':id', $uid);
    $res = $stm->execute();

    return boolval($res);
}

function update_password($uid, $password) {
    global $db_path;
    $db = new SQLite3($db_path);

    $hashed_password = password_hash($password, PASSWORD_BCRYPT);

    $stm = $db->prepare('UPDATE users set password = :password where id = :id');
    $stm->bindValue(':password', $hashed_password);
    $stm->bindValue(':id', $uid);
    $res = $stm->execute();

    return boolval($res);
}

function delete_user($uid) {
    global $db_path;
    $db = new SQLite3($db_path);

    $stm = $db->prepare('DELETE FROM users where id = :id');
    $stm->bindValue(':id', $uid);
    $res = $stm->execute();

    return boolval($res);
}

function get_user($uid) {
    global $db_path;
    $db = new SQLite3($db_path);

    $stm = $db->prepare('SELECT id, name, email, admin from users where id = :id');
    $stm->bindValue(':id', $uid);
    $res = $stm->execute();
    $row = $res->fetchArray();
    $user = array(
        "id" => $row['id'] ?? null,
        "name" => $row['name'] ?? null,
        "email" => $row['email'] ?? null,
        "admin" => $row['admin'] ?? null
    );
    return $user;
}

function verify_password($email, $password) {
    global $db_path;
    $db = new SQLite3($db_path);

    $stm = $db->prepare('SELECT id, name, password, admin from users where email = :email');
    $stm->bindValue(':email', $email);
    $res = $stm->execute();
    $row = $res->fetchArray();

    if (!$row) {
        return false;
    }

    $password_hash = $row['password'] ?? null;
    $uid = $row['id'] ?? null;
    $name = $row['name'] ?? null;
    $is_admin = boolval(intval($row['admin'] ?? 0) == 1);
    $password_match = password_verify($password, $password_hash);

    return array(
        "password_match" => $password_match,
        "uid" => $uid,
        "name" => $name,
        "is_admin" => $is_admin
    );
}

function add_steps($uid, $number_steps, $date_steps) {
    global $db_path;
    $db = new SQLite3($db_path);

    $stm = $db->prepare('INSERT INTO steps(uid, number_steps, date_steps) VALUES (:uid, :number_steps, :date_steps)');
    $stm->bindValue(':uid', $uid);
    $stm->bindValue(':number_steps', $number_steps);
    $stm->bindValue(':date_steps', $date_steps);
    $res = $stm->execute();

    return boolval($res);
}

function get_step($id, $uid) {
    global $db_path;
    $db = new SQLite3($db_path);

    $stm = $db->prepare('SELECT id, uid, number_steps, date_steps from steps where id = :id and uid = :uid');
    $stm->bindValue(':id', $id);
    $stm->bindValue(':uid', $uid);
    $res = $stm->execute();
    $row = $res->fetchArray();
    $step = array(
        "id" => $row['id'] ?? null,
        "number_steps" => $row['number_steps'] ?? null,
        "date_steps" => $row['date_steps'] ?? null
    );
    return $step;
}

function delete_step($id, $uid) {
    global $db_path;
    $db = new SQLite3($db_path);

    $stm = $db->prepare('DELETE from steps where id = :id and uid = :uid');
    $stm->bindValue(':id', $id);
    $stm->bindValue(':uid', $uid);
    $res = $stm->execute();
    // TODO: Check if it worked
}

function list_steps_for_user($uid) {
    global $db_path;
    $db = new SQLite3($db_path);

    $stm = $db->prepare('SELECT id, number_steps, date_steps from steps where uid = :uid order by date_steps desc');
    $stm->bindValue(':uid', $uid);
    $res = $stm->execute();
    $steps = array();
    while ($row = $res->fetchArray()) {
        $steps[] = array(
            "id" => $row['id'],
            "number_steps" => $row['number_steps'],
            "date_steps" => $row['date_steps']
        );
    }
    return $steps;
}

function get_sum_receipts_for_user($uid) {
    global $db_path;
    $db = new SQLite3($db_path);

    $stm = $db->prepare('SELECT sum(sum) as total_sum from receipts where uid = :uid and date_cleared is NULL');
    $stm->bindValue(':uid', $uid);
    $res = $stm->execute();
    $row = $res->fetchArray();
    $sum = $row['total_sum'];
    return $sum;
}

function get_number_receipts_for_user($uid) {
    global $db_path;
    $db = new SQLite3($db_path);

    $stm = $db->prepare('SELECT count(sum) as total_count from receipts where uid = :uid and date_cleared is NULL');
    $stm->bindValue(':uid', $uid);
    $res = $stm->execute();
    $row = $res->fetchArray();
    $count = $row['total_count'];
    return $count;
}

?>